import unittest
import log_message_pb2


class TestLogMessage(unittest.TestCase):
    def test_all_valid(self):
        lm = log_message_pb2.LogMessage()
        lm.log_level = "INFO"
        lm.logger = 'main'
        lm.mac = bytes([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
        lm.message = "This is the log message"

        self.assertTrue(lm.IsInitialized())

    def test_required_missing(self):
        lm = log_message_pb2.LogMessage()
        self.assertFalse(lm.IsInitialized())
        lm.message = "This is the log message"
        self.assertFalse(lm.IsInitialized())

    def test_valid_output(self):
        lm = log_message_pb2.LogMessage()
        lm.log_level = "DEBUG"
        lm.logger = 'main'
        lm.mac = bytes([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
        lm.message = "This is the log message"

        self.assertEqual(
            lm.SerializeToString(),
            b'\n\x05DEBUG\x12\x04main\x1a\x06\xaa\xbb\xcc\xdd\xee\xff"\x17This is the log message'
        )