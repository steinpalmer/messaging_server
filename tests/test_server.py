import logging
import socket
import struct
import threading
import unittest
from unittest.mock import patch
import log_message_pb2
from loggers import logger_name, logger_level
from server import make_server
from time import sleep


def make_message():
    lm = log_message_pb2.LogMessage()
    lm.log_level = 'DEBUG'
    lm.logger = 'main'
    lm.mac = bytes([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
    lm.message = "This is the log message"
    return lm.SerializeToString()


server_host, server_port = "localhost", 9999


class TestTCPRequestHandler(unittest.TestCase):
    def setUp(self):
        self.server = make_server(server_host, server_port)
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server_thread.start()
        self.client = socket.create_connection((server_host, server_port))

    def test_single_valid_message(self):
        payload = make_message()
        with patch('loggers.lm_logger.log') as patched_log:
            self.client.sendall(struct.pack('>L', len(payload)) + payload)
            # Artificially slowing down the test case here to allow the server to flush output before we read it in for
            # verification
            # This is an ugly hack for now
            sleep(0.1)
            patched_log.assert_called_once()
            patched_log.assert_called_with(
                logging.DEBUG,
                'This is the log message',
                extra={'mac': 'aa:bb:cc:dd:ee:ff', 'logger': 'main'}
            )

    def test_multiple_valid_messages(self):
        data = b''
        for _ in range(5):
            payload = make_message()
            data += struct.pack('>L', len(payload)) + payload

        with self.assertLogs(logger_name, level=logger_level) as cm:
            self.client.sendall(data)
            sleep(0.1)
            self.assertEqual(len(cm.output), 5)

    def test_insufficient_data_for_length(self):
        payload = make_message()
        data = struct.pack('>L', len(payload)) + payload

        with patch('loggers.lm_logger.log') as patched_log:
            self.client.sendall(data[:2])
            self.client.close()
            sleep(0.1)
            patched_log.assert_not_called()

    def tearDown(self):
        self.client.close()
        self.server.shutdown()
        self.server.server_close()
