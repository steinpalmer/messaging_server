# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains an implementation of the solution for the programming task given as part of the interview process with Boxine
* The server receives an arbitrary amount of protobuf-encoded, length-prefixed messages from connecting clients, 
converts the messages into properly formatted log messages and pass them to a logging system
* Version: 1.0

### How do I get set up? ###

* Clone this repository
* To setup the python environment and install dependencies(*nix systems): in a termainal, run `make setup_python_environment` 
* To run the tests: `make run_python_tests`
* Running the server: `make run_server`
* Running the demo client: `run_demo_client`
