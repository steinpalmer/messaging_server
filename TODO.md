#### Mental note of steps to implement task

- [x] Setup development environment
- [x] Install Protocol Buffers Compiler and python package dependencies (protobuff)
- [x] Write .proto file containing LogMessage specs
- [x] Generate Python classes (compile protocol buffers)
- [x] Write unittests to verify functionality of compiled output
- [x] Implement initial server functionality (synchronous)
- [x] Write unittests to verify that messages are being received by the server
- [x] Update server to read and parse messages received for completeness and correctness
- [x] Update the server implementation to handle requests asynchronously
