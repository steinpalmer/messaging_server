setup_python_environment:
	@echo 'Creating virtualenv for the project'
	@python3 -m venv --upgrade-deps venv
	@echo 'Installing project dependencies'
	@source venv/bin/activate && pip install -r requirements.txt
	@echo 'Done'

compile_protobuff_output_osx:
	@source venv/bin/activate && ./protobuff_compiler/osx/protoc/bin/protoc -I=src --python_out=src src/log_message.proto

run_python_tests:
	@source venv/bin/activate && python -m unittest

run_server:
	@source venv/bin/activate && python ./src/server.py

run_demo_client:
	@source venv/bin/activate && python ./src/demo_client.py