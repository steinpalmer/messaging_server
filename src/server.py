import logging
import socketserver
import struct
import log_message_pb2
from loggers import lm_logger
from google.protobuf.message import DecodeError


class TCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        """
        Every message block consists of an unsigned long (C type) denoting the length in bytes
        of the following serialized protobuf container.

        """
        struct_fmt = '>L'
        # 4 bytes but we call struct.calcsize in case struct_fmt changes later
        msg_len_size = struct.calcsize(struct_fmt)

        while True:
            # length in bytes of the following protobuf container
            message_len_packed = self.request.recv(msg_len_size)

            # Do not proceed if we fail to read sufficient bytes from the socket to determine
            # the size of the protobuf container
            if len(message_len_packed) < msg_len_size:
                break

            # Do not proceed if we cannot determine the size of the serialized protobuf container
            try:
                message_len_tuple = struct.unpack(struct_fmt, message_len_packed)

            except struct.error:
                break

            if len(message_len_tuple) < 1:
                break

            message_len = message_len_tuple[0]

            serialised_container = self.request.recv(message_len)

            # We do not know if the entire message block has been sent and available to read
            # Start reading bytes from the socket till we receive the entire message block
            while len(serialised_container) < message_len:
                remaining_bytes = message_len - len(serialised_container)
                serialised_container = serialised_container + self.request.recv(remaining_bytes)

            self.handle_log_message(serialised_container)

    def handle_log_message(self, serialised_container):
        log_message = log_message_pb2.LogMessage()

        # protobuf raises a DecodeError if the message cannot be parsed.
        # we silently handle that without affecting the processing of other message blocks
        try:
            log_message.ParseFromString(serialised_container)

        except DecodeError:
            return

        if not log_message.IsInitialized():
            return

        # dictionary mapping LogMessage's log_level name to python's logging level
        level_mapper = {
            'DEBUG': logging.DEBUG,
            'INFO': logging.INFO,
            'WARNING': logging.WARNING,
            'ERROR': logging.ERROR
        }

        level = level_mapper.get(log_message.log_level)

        # may only contain the values DEBUG, INFO, WARNING or ERROR
        if not level:
            return

        lm_logger.log(
            level,
            log_message.message,
            extra={'mac': log_message.mac.hex(':'), 'logger': log_message.logger}
        )


class LogMessageServer(socketserver.ThreadingTCPServer):
    allow_reuse_address = True


def make_server(host="127.0.0.1", port=15000):
    return LogMessageServer((host, port), TCPRequestHandler)


if __name__ == "__main__":
    with make_server() as server:
        print('server is running in thread')
        server.serve_forever()
