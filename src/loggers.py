import logging
import sys


logger_name = 'log_message_server'
logger_level = logging.DEBUG

# Create the console (stdout) handler and set the level
console = logging.StreamHandler(sys.stdout)
console.setLevel(logger_level)

# Create the format of the log messages and append it to the console handler
formatter = logging.Formatter('%(levelname)s: %(logger)s %(mac)s %(message)s')
console.setFormatter(formatter)

# Create the logger and set the level
lm_logger = logging.getLogger(logger_name)
lm_logger.setLevel(logger_level)

# Attach the console handler to the lm_logger
lm_logger.addHandler(console)
